from fastapi import FastAPI, Depends
from fastapi_users import FastAPIUsers

from auth.auth import auth_backend
from database import User
from auth.shemas import UserRead, UserCreate
from auth.user_manager import get_user_manager

app = FastAPI(title="Salary")

fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

# Предоставляет эндпоинты для входа и выхода из системы (login/logout)
app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["auth"],
)

# Предоставляет эндпоинт для регистрации пользователя (добавление в базу данных)
app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)


current_user = fastapi_users.current_user()


@app.get("/get_userdata")
def protected_route(user: User = Depends(current_user)):
    """
    Защищенный эндпоинт для получения данных о пользователе.
    Вернет данные только при условии входа в систему (login) и действующем токене
    """
    return {
        "username": user.username,
        "salary": user.salary,
        "salary increase date": user.salary_increase_date
    }
