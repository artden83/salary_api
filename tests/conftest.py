from typing import AsyncGenerator
import asyncio
import pytest
from httpx import AsyncClient
from sqlalchemy import NullPool, insert
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from starlette.testclient import TestClient

from models.models import metadata, user
from database import get_async_session
from config import DB_USER_TEST, DB_PASS_TEST, DB_HOST_TEST, DB_PORT_TEST, DB_NAME_TEST
from salary import app

# TEST DATABASE
DATABASE_URL_TEST = f"postgresql+asyncpg://{DB_USER_TEST}:{DB_PASS_TEST}@{DB_HOST_TEST}:{DB_PORT_TEST}/{DB_NAME_TEST}"


engine_test = create_async_engine(DATABASE_URL_TEST, poolclass=NullPool)
async_session_maker = sessionmaker(engine_test, class_=AsyncSession, expire_on_commit=False)

# переопределение метаданных в тестовую базу
metadata.bind = engine_test



async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    """ функция создает асинхронную сессию"""
    async with async_session_maker() as session:
        yield session

app.dependency_overrides[get_async_session] = override_get_async_session


@pytest.fixture(autouse=True, scope='session')
async def prepare_database():
    """
    Фикстура создает базу данных и одну запись в ней, после чего удаляет базу после выполнения всех тестов,
    metadata отдает свои значения для построения таблиц
    """
    async with engine_test.begin() as conn:
        await conn.run_sync(metadata.create_all)
    async with async_session_maker() as session:
        stmt = insert(user).values(
            id=1,
            username="Antonov A.A.",
            email="antonov@mai.ru",
            salary=15000,
            salary_increase_date="2023-08-22",
            hashed_password="123",
        )
        await session.execute(stmt)
        await session.commit()
    yield
    async with engine_test.begin() as conn:
        await conn.run_sync(metadata.drop_all)


# SETUP из документации pytest-asyncio
@pytest.fixture(scope='session')
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


# Синхронный клиент
client = TestClient(app)


@pytest.fixture(scope="session")
async def ac() -> AsyncGenerator[AsyncClient, None]:
    """ Создание асинхронного клиента для асинхронных запросов к базе данных """
    async with AsyncClient(app=app, base_url="http://test") as ac:
        yield ac
