from httpx import AsyncClient
from sqlalchemy import select
from models.models import user
from tests.conftest import async_session_maker


async def test_get_user():
    """
        проверка получения данных из базы
    """
    async with async_session_maker() as session:
        query = select(user)
        result = await session.execute(query)
        assert result.all() == [(1, 'Antonov A.A.', 'antonov@mai.ru', 15000, '2023-08-22', '123', True, False, False)]


async def test_401_without_token(ac: AsyncClient):
    """ Возвращает ошибку 401 если токен отсутствует (или время его действия истекло)"""
    response = await ac.get("/get_userdata")
    assert response.status_code == 401
