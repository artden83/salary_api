from fastapi_users.authentication import CookieTransport, JWTStrategy, AuthenticationBackend

cookie_transport = CookieTransport(cookie_name="salary", cookie_max_age=300)

# Секртный ключ оставлен как в документации, для удобства
SECRET = "SECRET"


def get_jwt_strategy() -> JWTStrategy:
    """
    JSON Web Token (JWT) — это интернет-стандарт для создания токенов доступа на основе JSON. Их не нужно хранить
    в базе данных: данные самодостаточны внутри и криптографически подписаны.
    lifetime_seconds определяет время действия токена в секундах
    """
    return JWTStrategy(secret=SECRET, lifetime_seconds=300)


auth_backend = AuthenticationBackend(
    name="jwt",
    transport=cookie_transport,
    get_strategy=get_jwt_strategy,
)
